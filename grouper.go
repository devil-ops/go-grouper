/*
Package grouper interacts with the grouper API
*/
package grouper

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"moul.io/http2curl"
)

const userAgent = "go-grouper"

// Client is the thing we use to connect and hold all the methods
type Client struct {
	client    *http.Client
	username  string
	password  string
	baseurl   string
	debugCurl bool
	// ldapClient ldap.Client
	// baseDN         string
	groupsEndpoint string
}

/*
// WithBaseDN sets the base DN for ldap searches
func WithBaseDN(s string) func(*Client) {
	return func(c *Client) {
		c.baseDN = s
	}
}
*/

// WithBaseURL sets the base URL for the grouper API
func WithBaseURL(s string) func(*Client) {
	return func(c *Client) {
		c.baseurl = s
	}
}

/*
// WithLDAP sets the ldap client
func WithLDAP(l ldap.Client) func(*Client) {
	return func(c *Client) {
		c.ldapClient = l
	}
}
*/

// WithDebugCurl will determin if the curl commands are printed when doing requests
func WithDebugCurl() func(*Client) {
	return func(c *Client) {
		c.debugCurl = true
	}
}

// WithUsername sets the authentication username for a grouper client
func WithUsername(s string) func(*Client) {
	return func(c *Client) {
		c.username = s
	}
}

// WithPassword sets the authentication password for a grouper client
func WithPassword(s string) func(*Client) {
	return func(c *Client) {
		c.password = s
	}
}

// MustNew returns a new client or panics if there is an error
func MustNew(opts ...func(*Client)) *Client {
	got, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return got
}

// New returns a new Client and an optional error
func New(opts ...func(*Client)) (*Client, error) {
	c := &Client{
		client:  http.DefaultClient,
		baseurl: "https://groups.oit.duke.edu/grouper-ws/servicesRest/v2_1_500/",
	}

	for _, opt := range opts {
		opt(c)
	}
	if c.username == "" {
		return nil, errors.New("must set username")
	}
	if c.password == "" {
		return nil, errors.New("must set password")
	}

	// Set some additional urls and do some cleanup
	c.baseurl = strings.TrimSuffix(c.baseurl, "/")
	c.groupsEndpoint = c.baseurl + "/groups"
	return c, nil
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*http.Response, error) {
	req.Header.Set("Content-Type", "text/x-json")
	req.Header.Set("User-Agent", userAgent)
	req.SetBasicAuth(c.username, c.password)

	if c.debugCurl {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprintf(os.Stderr, "%v\n", command)
	}

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		/*
			var errRes errorResponse
			if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
				return nil, errors.New(errRes.WsRestResultProblem.ResultMetadata.ResultMessage)
			}
		*/
		var errRes ErrorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			if realErr, eerr := errRes.extractErr(); eerr == nil {
				return nil, errors.New(strings.ReplaceAll(realErr.ResultMessage, "\n", " "))
			}
			return nil, errors.New("could not figure out what kind of error happened 😭")
		}

		return nil, fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	if err = json.NewDecoder(res.Body).Decode(&v); err != nil {
		return nil, err
	}

	return res, nil
}

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		fmt.Fprintf(os.Stderr, "error closing item: %v", err)
	}
}

/*
// User returns information about a given user
//
// Deprecated: Use Subject() instead
func (c *Client) User(s string) (*User, error) {
	searchAttr := "uid"
	if !isNetID(s) {
		searchAttr = "duDukeID"
	}
	u, err := c.userWith(s, searchAttr)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (c *Client) userWith(s string, a string) (*User, error) {
	searchReq := ldap.NewSearchRequest(
		c.baseDN,
		ldap.ScopeWholeSubtree,
		0, 0, 0,
		false,
		fmt.Sprintf(`(%s=%s)`, a, ldap.EscapeFilter(s)),
		[]string{"duDukeID", "uid", "displayName"},
		[]ldap.Control{},
	)
	result, err := c.ldapClient.Search(searchReq)
	if err != nil {
		return nil, err
	}
	if len(result.Entries) != 1 {
		return nil, fmt.Errorf("must return exactly 1 error, got: %v", len(result.Entries))
	}
	u := &User{}
	for _, item := range result.Entries[0].Attributes {
		switch item.Name {
		case "displayName":
			u.Name = item.Values[0]
		case "duDukeID":
			u.ID = item.Values[0]
		case "uid":
			u.NetID = item.Values[0]
		}
	}
	return u, nil
}
*/

// Remove removes a group from grouper
func (c *Client) Remove(r WsRestGroupDeleteRequest) (*WsGroupDeleteResults, error) {
	reqB, err := json.Marshal(DeleteGroupRequestWrapper{
		WsRestGroupDeleteRequest: r,
	})
	if err != nil {
		return nil, err
	}

	var ret RemoveGroupResponseWrapper
	resp, err := c.sendRequest(mustNewPostRequest(c.groupsEndpoint, bytes.NewReader(reqB)), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	return &ret.WsGroupDeleteResults, nil
}

// RemoveMember removes a member or members from a group
func (c *Client) RemoveMember(g string, r WsRestDeleteMemberRequest) (*WsDeleteMemberResults, error) {
	reqB, err := json.Marshal(DeleteMemberRequestWrapper{
		WsRestDeleteMemberRequest: r,
	})
	if err != nil {
		return nil, err
	}

	var ret RemoveMemberResultsWrapper
	resp, err := c.sendRequest(mustNewPostRequest(c.groupsEndpoint+"/"+g+"/members", bytes.NewReader(reqB)), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	return &ret.WsDeleteMemberResults, nil
}

// Create adds a new group to grouper
func (c *Client) Create(g string, r WsRestGroupSaveLiteRequest) (*WsGroupSaveLiteResult, error) {
	reqB := mustMarshal(AddGroupRequestWrapper{
		WsRestGroupSaveLiteRequest: r,
	})

	var ret AddGroupResponseWrapper
	resp, err := c.sendRequest(mustNewPostRequest(c.groupsEndpoint+"/"+g, bytes.NewReader(reqB)), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	return &ret.WsGroupSaveLiteResult, nil
}

// AddMember takes a fully qualified group name and a WsRestAddMemberRequest to add members to a group
func (c *Client) AddMember(g string, r WsRestAddMemberRequest) (*WsAddMemberResults, error) {
	reqB, err := json.Marshal(AddMemberRequestWrapper{
		WsRestAddMemberRequest: r,
	})
	if err != nil {
		return nil, err
	}

	var ret AddMemberResultsWrapper
	resp, err := c.sendRequest(mustNewPostRequest(c.groupsEndpoint+"/"+g+"/members", bytes.NewReader(reqB)), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	return &ret.WsAddMemberResults, nil
}

// SetPrivilege adds a privilege to a given group
func (c *Client) SetPrivilege(r WsRestAssignGrouperPrivilegesRequest) (*WsAssignGrouperPrivilegesResults, error) {
	reqB := mustMarshal(AssignPrivilegesRequestWrapper{
		WsRestAssignGrouperPrivilegesRequest: r,
	})

	var ret AssignPrivilegesResponseWrapper
	resp, err := c.sendRequest(mustNewPostRequest(c.baseurl+"/grouperPrivileges", bytes.NewReader(reqB)), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	return &ret.WsAssignGrouperPrivilegesResults, nil
}

// Members returns member information about a group
func (c *Client) Members(filter WsRestGetMembersRequest) ([]WsGetMembersResultsResult, error) {
	reqB := mustMarshal(WsRestGetMembersRequestWrapper{
		WsRestGetMembersRequest: filter,
	})

	var ret MemberResultsWrapper
	resp, err := c.sendRequest(mustNewPostRequest(c.groupsEndpoint+"/", bytes.NewReader(reqB)), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	return ret.WsGetMembersResults.Results, nil
}

// Privileges returns privilege information about a group
func (c *Client) Privileges(r WsRestGetGrouperPrivilegesLiteRequest) (*WsGetGrouperPrivilegesLiteResult, error) {
	reqB := mustMarshal(ListPrivilegesRequestWrapper{
		WsRestGetGrouperPrivilegesLiteRequest: r,
	})

	var ret GetPrivilegesResponseWrapper
	resp, err := c.sendRequest(mustNewPostRequest(c.baseurl+"/grouperPrivileges", bytes.NewReader(reqB)), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	return &ret.WsGetGrouperPrivilegesLiteResult, nil
}

// SearchGroups searches groups
func (c *Client) SearchGroups(filter WsQueryFilter) ([]Group, error) {
	reqB, err := json.Marshal(WsRestFindGroupsRequestWrapper{
		WsRestFindGroupsRequest: WsRestFindGroupsRequest{
			WsQueryFilter: filter,
		},
	})
	if err != nil {
		return nil, err
	}

	var ret WsFindGroupsResultsResponse
	resp, err := c.sendRequest(mustNewPostRequest(c.groupsEndpoint+"/", bytes.NewReader(reqB)), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	return ret.WsFindGroupsResults.GroupResults, nil
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

// User represents some minimal amount of information about a person
type User struct {
	Name  string
	NetID string
	ID    string
}

// Subject returns a subject from the API
func (c *Client) Subject(by, value string) (*WsGetSubjectsResults, error) {
	if by == "" || value == "" {
		return nil, errors.New("must not pass an empty string to by or value")
	}
	var ret GetSubjectResponseWrapper
	resp, err := c.sendRequest(mustNewPostRequest(c.baseurl+"/subjects/?SubjectAttributeNames=uid&wsLiteObjectType=WsRestGetSubjectsLiteRequest&"+by+"="+value, nil), &ret)
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	totalRes := len(ret.WsGetSubjectsResults.WsSubjects)
	switch totalRes {
	case 0:
		return nil, errors.New("no subjects found")
	case 1:
		if ret.WsGetSubjectsResults.WsSubjects[0].ResultCode != "SUCCESS" {
			return nil, errors.New(ret.WsGetSubjectsResults.WsSubjects[0].ResultCode)
		}
		return &ret.WsGetSubjectsResults, nil
	default:
		return nil, fmt.Errorf("expected only 1 result, but got back: %v", totalRes)
	}
}
