# Go Grouper

Library to interact with Grouper in Go, and a CLI app called `grouperctl` based on Jeremy Thornhill's awesome python script

## Using GrouperCTL

Get a username and password to authenticate with, and set them in your environment variables:

```bash
export GROUPERCTL_USERNAME=01234
export GROUPERCTL_PASSWORD=my-password
```

**Note** that the username is actually a Duke unique ID, not a NetID

You can also set `GROUPERCTL_STEM` environment variable to a given stem if you don't want to use them in your commands.

## Installation

Binaries can be downloaded from the [release](https://gitlab.oit.duke.edu/devil-ops/go-skrumpy/-/releases) page, or you can use the [devil-ops packaging](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages) to install the package.

Note the binary is `grouperctl`, so you can use that name with homebrew (Example: `brew install grouperctl`)
