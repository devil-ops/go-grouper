package grouper

import (
	"errors"
	"fmt"
	"strings"
)

// WsFindGroupsResultsResponse is what we get back from the API when searching groups
type WsFindGroupsResultsResponse struct {
	WsFindGroupsResults WsFindGroupsResults
}

// WsFindGroupsResults are the group results with metadata from a group find
type WsFindGroupsResults struct {
	GroupResults     []Group          `json:"groupResults,omitempty"`
	ResponseMetadata ResponseMetadata `json:"responseMetadata,omitempty"`
	ResultMetadata   ResultMetadata   `json:"resultMetadata,omitempty"`
}

// Group are the items returned from a group query
type Group struct {
	Description      string `json:"description,omitempty"`
	DisplayExtension string `json:"displayExtension,omitempty"`
	DisplayName      string `json:"displayName,omitempty"`
	Enabled          string `json:"enabled,omitempty"`
	Extension        string `json:"extension,omitempty"`
	Name             string `json:"name,omitempty"`
	TypeOfGroup      string `json:"typeOfGroup,omitempty"`
	UUID             string `json:"uuid,omitempty"`
}

// GroupList represents multiple Group items
type GroupList []Group

// Summary summarizes a GroupList
func (g GroupList) Summary() string {
	ret := strings.Builder{}
	for _, group := range g {
		ret.WriteString(
			strings.Join([]string{"GRP", group.UUID, group.Name}, ",") + "\n",
		)
	}
	return ret.String()
}

// ResultMetadata is result metadata
type ResultMetadata struct {
	ResultCode    string `json:"resultCode,omitempty"`
	ResultMessage string `json:"resultMessage,omitempty"`
	Success       string `json:"success,omitempty"`
}

// ResponseMetadata is metadata returned with each response
type ResponseMetadata struct {
	Millis        string `json:"millis,omitempty"`
	ServerVersion string `json:"serverVersion,omitempty"`
}

// MemberResultsWrapper is what is returned when looking up group members
type MemberResultsWrapper struct {
	WsGetMembersResults WsGetMembersResults
}

// WsGetMembersResults are the member results
type WsGetMembersResults struct {
	ResponseMetadata      ResponseMetadata            `json:"responseMetadata,omitempty"`
	ResultMetadata        ResultMetadata              `json:"resultMetadata,omitempty"`
	Results               []WsGetMembersResultsResult `json:"results,omitempty"`
	SubjectAttributeNames []string                    `json:"subjectAttributeNames,omitempty"`
}

// WsGetMembersResultsResult are where the actual results are I guess
type WsGetMembersResultsResult struct {
	ResultMetadata ResultMetadata `json:"resultMetadata,omitempty"`
	WsGroup        WsGroup        `json:"wsGroup,omitempty"`
	WsSubjects     WsSubjectList  `json:"wsSubjects,omitempty"`
}

// Summary provides a brief summary of the group results
func (r WsGetMembersResultsResult) Summary() string {
	ret := strings.Builder{}
	for _, item := range r.WsSubjects {
		ret.WriteString(item.MarshalLine())
	}
	return ret.String()
}

// WsSubjectList represents multiple WsSubject items
type WsSubjectList []WsSubject

// Summary returns a summary of a given WsGroupList
func (w WsSubjectList) Summary() string {
	ret := strings.Builder{}
	for _, item := range w {
		ret.WriteString(item.MarshalLine())
	}
	return ret.String()
}

// WsSubject is the member result
type WsSubject struct {
	AttributeValues []string `json:"attributeValues,omitempty"`
	ID              string   `json:"id,omitempty"`
	MemberID        string   `json:"memberId,omitempty"`
	Name            string   `json:"name,omitempty"`
	ResultCode      string   `json:"resultCode,omitempty"`
	SourceID        string   `json:"sourceId,omitempty"`
	Success         string   `json:"success,omitempty"`
}

// MarshalLine returns the subject in a simple single line format, similar to Jeremy's awesome python script
func (s WsSubject) MarshalLine() string {
	if s.SourceID == "jndiperson" {
		return strings.Join([]string{"USR", s.ID, s.AttributeValues[0], s.Name}, ",") + "\n"
	}
	return strings.Join([]string{"GRP", s.ID, s.Name}, ",") + "\n"
}

// WsGroup is a gruop
type WsGroup struct {
	Description      string `json:"description,omitempty"`
	DisplayExtension string `json:"displayExtension,omitempty"`
	DisplayName      string `json:"displayName,omitempty"`
	Enabled          string `json:"enabled,omitempty"`
	Extension        string `json:"extension,omitempty"`
	Name             string `json:"name,omitempty"`
	TypeOfGroup      string `json:"typeOfGroup,omitempty"`
	UUID             string `json:"uuid,omitempty"`
}

// WsGroupList represents multiple WsGroup items
type WsGroupList []WsGroup

// Summary returns a summary of a given WsGroupList
func (w WsGroupList) Summary() string {
	ret := strings.Builder{}
	for _, item := range w {
		ret.WriteString(strings.Join([]string{item.UUID, item.Name}, ",") + "\n")
	}
	return ret.String()
}

// AddMemberResultsWrapper is what we get back directly from the API
type AddMemberResultsWrapper struct {
	WsAddMemberResults WsAddMemberResults `json:"WsAddMemberResults,omitempty"`
}

// WsAddMemberResults is the actual result stuff I think?
type WsAddMemberResults struct {
	ResponseMetadata ResponseMetadata         `json:"responseMetadata,omitempty"`
	ResultMetadata   ResultMetadata           `json:"resultMetadata,omitempty"`
	Results          []AddMemberResultsResult `json:"results,omitempty"`
	WsGroupAssigned  Group                    `json:"wsGroupAssigned,omitempty"`
}

// AddMemberResultsResult is the actual results in side the result response
type AddMemberResultsResult struct {
	ResultMetadata ResultMetadata `json:"resultMetadata,omitempty"`
	WsSubject      WsSubject      `json:"wsSubject,omitempty"`
}

// RemoveMemberResultsWrapper is what we get back from the API
type RemoveMemberResultsWrapper struct {
	WsDeleteMemberResults WsDeleteMemberResults
}

// WsDeleteMemberResults is the delete member results
type WsDeleteMemberResults struct {
	ResponseMetadata ResponseMetadata              `json:"responseMetadata,omitempty"`
	ResultMetadata   ResultMetadata                `json:"resultMetadata,omitempty"`
	Results          []WsDeleteMemberResultsResult `json:"results,omitempty"`
	WsGroup          WsGroup                       `json:"wsGroup,omitempty"`
}

// WsDeleteMemberResultsResult are the actual result items
type WsDeleteMemberResultsResult struct {
	ResultMetadata ResultMetadata `json:"resultMetadata,omitempty"`
	WsSubject      WsSubject      `json:"wsSubject,omitempty"`
}

// AddGroupResponseWrapper is the data we get back from the API endpoint for adding a group
type AddGroupResponseWrapper struct {
	WsGroupSaveLiteResult WsGroupSaveLiteResult
}

// WsGroupSaveLiteResult the juicy part of the response
type WsGroupSaveLiteResult struct {
	ResponseMetadata ResponseMetadata `json:"responseMetadata,omitempty"`
	ResultMetadata   ResultMetadata   `json:"resultMetadata,omitempty"`
	WsGroup          WsGroup          `json:"wsGroup,omitempty"`
}

// RemoveGroupResponseWrapper is what we get back directly from the api
type RemoveGroupResponseWrapper struct {
	WsGroupDeleteResults WsGroupDeleteResults
}

// WsGroupDeleteResults is the actual results
type WsGroupDeleteResults struct {
	ResponseMetadata ResponseMetadata             `json:"responseMetadata,omitempty"`
	ResultMetadata   ResultMetadata               `json:"resultMetadata,omitempty"`
	Results          []WsGroupDeleteResultsResult `json:"results,omitempty"`
}

// WsGroupDeleteResultsResult is the actual result item
type WsGroupDeleteResultsResult struct {
	ResultMetadata ResultMetadata `json:"resultMetadata,omitempty"`
	WsGroup        WsGroup        `json:"wsGroup,omitempty"`
}

// GetPrivilegesResponseWrapper is what we get back directly from the API
type GetPrivilegesResponseWrapper struct {
	WsGetGrouperPrivilegesLiteResult WsGetGrouperPrivilegesLiteResult
}

// WsGetGrouperPrivilegesLiteResult is the juice of the response
type WsGetGrouperPrivilegesLiteResult struct {
	PrivilegeResults PrivilegeResultList `json:"privilegeResults,omitempty"`
	ResponseMetadata ResponseMetadata    `json:"responseMetadata,omitempty"`
	ResultMetadata   ResultMetadata      `json:"resultMetadata,omitempty"`
}

// PrivilegeResultList represents multiple PrivilegeResult items
type PrivilegeResultList []PrivilegeResult

// Summary returns a summary of privilege results
func (p PrivilegeResultList) Summary() string {
	ret := strings.Builder{}
	for _, item := range p {
		ret.WriteString(strings.Join([]string{"PRIV", item.OwnerSubject.ID, item.OwnerSubject.Name, item.PrivilegeName, item.PrivilegeType}, ",") + "\n")
	}
	return ret.String()
}

// PrivilegeResult is the actual result for what a privilege is
type PrivilegeResult struct {
	Allowed       string       `json:"allowed,omitempty"`
	OwnerSubject  OwnerSubject `json:"ownerSubject,omitempty"`
	PrivilegeName string       `json:"privilegeName,omitempty"`
	PrivilegeType string       `json:"privilegeType,omitempty"`
	Revokable     string       `json:"revokable,omitempty"`
	WsGroup       WsGroup      `json:"wsGroup,omitempty"`
	WsSubject     WsSubject    `json:"wsSubject,omitempty"`
}

// OwnerSubject represents an owner I guess
type OwnerSubject struct {
	ID         string `json:"id,omitempty"`
	Name       string `json:"name,omitempty"`
	ResultCode string `json:"resultCode,omitempty"`
	SourceID   string `json:"sourceId,omitempty"`
	Success    string `json:"success,omitempty"`
}

// ErrorResponse is a helper item to normalize the error responses returned. We have to do this, because the top field returned in errors is going to be unique per request type
// type ErrorResponse map[string]map[string]ResultMetadata
type ErrorResponse map[string]map[string]any

func (e ErrorResponse) extractErr() (*ResultMetadata, error) {
	for _, v := range e {
		for k, item := range v {
			if k == "resultMetadata" {
				ritem := item.(map[string]interface{})
				ret := &ResultMetadata{
					ResultCode:    fmt.Sprint(ritem["resultCode"]),
					ResultMessage: fmt.Sprint(ritem["resultMessage"]),
					Success:       fmt.Sprint(ritem["success"]),
				}
				return ret, nil
			}
		}
	}
	return nil, errors.New("could not extract error metadata")
}

// AssignPrivilegesResponseWrapper is what we get back directly from the API
type AssignPrivilegesResponseWrapper struct {
	WsAssignGrouperPrivilegesResults WsAssignGrouperPrivilegesResults
}

// WsAssignGrouperPrivilegesResults is the juicy part of the response
type WsAssignGrouperPrivilegesResults struct {
	ResponseMetadata ResponseMetadata                         `json:"responseMetadata,omitempty"`
	ResultMetadata   ResultMetadata                           `json:"resultMetadata,omitempty"`
	Results          []WsAssignGrouperPrivilegesResultsResult `json:"results,omitempty"`
	WsGroup          WsGroup                                  `json:"wsGroup,omitempty"`
}

// WsAssignGrouperPrivilegesResultsResult is the actual result item
type WsAssignGrouperPrivilegesResultsResult struct {
	PrivilegeName  string         `json:"privilegeName,omitempty"`
	PrivilegeType  string         `json:"privilegeType,omitempty"`
	ResultMetadata ResultMetadata `json:"resultMetadata,omitempty"`
	WsSubject      WsSubject      `json:"wsSubject,omitempty"`
}

// GetSubjectResponseWrapper is exactly what the API returns when requesting a subject
type GetSubjectResponseWrapper struct {
	WsGetSubjectsResults WsGetSubjectsResults
}

// WsGetSubjectsResults are the actual subject results
type WsGetSubjectsResults struct {
	ResponseMetadata ResponseMetadata `json:"responseMetadata,omitempty"`
	ResultMetadata   ResultMetadata   `json:"resultMetadata,omitempty"`
	WsSubjects       WsSubjectList    `json:"wsSubjects,omitempty"`
}
