package grouper

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func testCopyFile(f string, w io.Writer) {
	rp, err := os.ReadFile(f)
	panicIfErr(err)
	_, err = io.Copy(w, bytes.NewReader(rp))
	panicIfErr(err)
}

func TestNew(t *testing.T) {
	// No username or password
	got, err := New()
	require.Nil(t, got)
	require.EqualError(t, err, "must set username")

	// No password
	got, err = New(WithUsername("foo"))
	require.Nil(t, got)
	require.EqualError(t, err, "must set password")

	// Username and Password
	got, err = New(WithUsername("foo"), WithPassword("bar"))
	require.NotNil(t, got)
	require.NoError(t, err)
}

func TestFindGroups(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/groupresults.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)
	got, err := c.SearchGroups(WsQueryFilter{
		GroupName:       "%",
		QueryFilterType: "FIND_BY_GROUP_NAME_APPROXIMATE",
		StemName:        "foo",
	})
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, 2, len(got))
}

func TestListMembers(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/lsmembers.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)

	got, err := c.Members(WsRestGetMembersRequest{})
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Greater(t, len(got), 0)
}

func TestAddMember(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/addmember.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)

	got, err := c.AddMember("foo:bar", WsRestAddMemberRequest{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestRemoveMember(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/rmmember.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)

	got, err := c.RemoveMember("foo:bar", WsRestDeleteMemberRequest{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestAdd(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/addmember.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)

	got, err := c.Create("foo:bar", WsRestGroupSaveLiteRequest{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestRemove(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/rmgroup.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)

	got, err := c.Remove(WsRestGroupDeleteRequest{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestPrivileges(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/getprivs.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)

	got, err := c.Privileges(WsRestGetGrouperPrivilegesLiteRequest{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestErrorUnmarshal(t *testing.T) {
	errFiles := []string{
		"./testdata/priverror.json",
		"./testdata/memberserr.json",
	}
	for _, errFile := range errFiles {
		err1b, err := os.ReadFile(errFile)
		require.NoError(t, err)
		var err1 ErrorResponse
		// gout.MustPrint(string(err1b))
		require.NoError(t, json.Unmarshal(err1b, &err1))
		rerr, err := err1.extractErr()
		require.NoError(t, err)
		// panic("foo")
		require.NoError(t, err)
		require.NotEmpty(t, rerr.ResultCode)
	}
}

func TestAddPrivileges(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/setprivs.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)

	got, err := c.SetPrivilege(WsRestAssignGrouperPrivilegesRequest{})
	require.NoError(t, err)
	require.NotNil(t, got)
}

func TestSubject(t *testing.T) {
	tsrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		testCopyFile("./testdata/getsubject.json", w)
	}))
	c := MustNew(
		WithDebugCurl(),
		WithBaseURL(tsrv.URL),
		WithUsername("foo"),
		WithPassword("bar"),
	)

	got, err := c.Subject("field", "1234")
	require.NoError(t, err)
	require.NotNil(t, got)
}
