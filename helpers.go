package grouper

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
)

func mustNewRequest(method string, url string, body io.Reader) *http.Request {
	got, err := http.NewRequest(method, url, body)
	if err != nil {
		panic(err)
	}
	return got
}

func mustNewPostRequest(url string, body io.Reader) *http.Request {
	return mustNewRequest("POST", url, body)
}

func isNetID(s string) bool {
	_, err := strconv.Atoi(s)
	return err != nil
}

func mustMarshal(v any) []byte {
	got, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return got
}
