package grouper

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIsNetID(t *testing.T) {
	require.True(t, isNetID("foo"))
	require.False(t, isNetID("1234"))
}
