/*
Package cmd holds all the cli stuff
*/
package cmd

import (
	"log/slog"
	"os"
	"time"

	"github.com/charmbracelet/log"
	goutbra "github.com/drewstinnett/gout-cobra"
	homedir "github.com/mitchellh/go-homedir"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	client  *grouper.Client
	logger  *slog.Logger
	verbose bool
	version = "dev"
)

// rootCmd represents the base command when called without any subcommands
func newRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "grouperctl",
		Short:         "Interact with the Grouper API",
		SilenceErrors: true,
		SilenceUsage:  true,
		Version:       version,
		PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
			initConfig()

			logger = slog.New(log.NewWithOptions(os.Stderr, newLoggerOpts()))
			slog.SetDefault(logger)

			if gerr := goutbra.Cmd(cmd); gerr != nil {
				return gerr
			}

			var err error
			opts := []func(*grouper.Client){
				grouper.WithUsername(viper.GetString("username")),
				grouper.WithPassword(viper.GetString("password")),
			}
			if os.Getenv("DEBUG_CURL") != "" {
				opts = append(opts, grouper.WithDebugCurl())
			}
			client, err = grouper.New(opts...)
			return err
		},
	}
	cmd.PersistentFlags().String("username", "", "Username for API authentication")
	cmd.PersistentFlags().String("password", "", "Password for API authentication")
	cmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Enable verbose logging")
	cmd.PersistentFlags().Bool("skip-confirm", false, "Skip any confirmation boxes that would normally pop up")
	cmd.AddCommand(
		newGetCmd(),
		newCreateCmd(),
		newRemoveCmd(),
	)

	panicIfErr(goutbra.Bind(cmd))
	panicIfErr(viper.BindPFlags(cmd.Flags()))
	return cmd
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := newRootCmd().Execute()
	if err != nil {
		slog.Error("fatal error occurred", "error", err)
		os.Exit(1)
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".planispherectl" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".grouperctl")
	}

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix("grouperctl")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		slog.Debug("using config file", "file", viper.ConfigFileUsed())
	}
}

func newLoggerOpts() log.Options {
	logOpts := log.Options{
		ReportTimestamp: true,
		TimeFormat:      time.Kitchen,
		Prefix:          "grouperctl 🐠 ",
		Level:           log.InfoLevel,
	}
	if verbose {
		logOpts.Level = log.DebugLevel
	}

	return logOpts
}
