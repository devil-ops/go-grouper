package cmd

import (
	"github.com/spf13/cobra"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newCreateGroupsCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "group NAME",
		Aliases: []string{"m", "members"},
		Short:   "Create a new Grouper group",
		Args:    cobra.ExactArgs(1),
		RunE:    runCreateGroups,
	}
	cmd.PersistentFlags().StringP("description", "d", "", "Description for the group")
	cmd.PersistentFlags().StringP("display-name", "l", "", "Display name for the group")
	return cmd
}

func runCreateGroups(cmd *cobra.Command, args []string) error {
	gi, err := qualify(args[0])
	if err != nil {
		return err
	}
	res, err := client.Create(gi.FullName(), *grouper.NewCreateGroupRequest(
		grouper.WithGroupName[grouper.WsRestGroupSaveLiteRequest](gi.FullName()),
		grouper.WithDescription(mustGetCmd[string](*cmd, "description")),
		grouper.WithDisplayExtension(mustGetCmd[string](*cmd, "display-name")),
	))
	if err != nil {
		return err
	}
	logger.Info("completed", "code", res.ResultMetadata.ResultCode)

	return nil
}
