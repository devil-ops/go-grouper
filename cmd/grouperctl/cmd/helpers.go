package cmd

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/charmbracelet/huh"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func mustGetCmd[T []int | int | []string | string | bool | time.Duration](cmd cobra.Command, s string) T {
	switch any(new(T)).(type) {
	case *int:
		item, err := cmd.Flags().GetInt(s)
		panicIfErr(err)
		return any(item).(T)
	case *string:
		item, err := cmd.Flags().GetString(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]string:
		item, err := cmd.Flags().GetStringArray(s)
		panicIfErr(err)
		return any(item).(T)
	case *bool:
		item, err := cmd.Flags().GetBool(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]int:
		item, err := cmd.Flags().GetIntSlice(s)
		panicIfErr(err)
		return any(item).(T)
	case *time.Time:
		item, err := cmd.Flags().GetDuration(s)
		panicIfErr(err)
		return any(item).(T)
	default:
		panic(fmt.Sprintf("unexpected use of mustGetCmd: %v", reflect.TypeOf(s)))
	}
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

// fqGroupName returns the fully qualified group name and an optional error
func fqGroupName(n string) (string, error) {
	if strings.Contains(n, ":") {
		return n, nil
	}
	stem := viper.GetString("stem")
	if stem == "" {
		return "", errors.New("must include a full stem (include :)")
	}
	return stem + ":" + n, nil
}

// fqGroupSplit returns the stem and group name from a given fully qualified group
func fqGroupSplit(n string) (string, string) {
	i := strings.LastIndex(n, ":")
	return n[:i], n[i+1:]
}

// qualifyAndSplit returns the stem and name from a given name
//
// Deprecated: use qualify(name) instead
func qualifyAndSplit(n string) (string, string, error) {
	fq, err := fqGroupName(n)
	if err != nil {
		return "", "", err
	}
	stem, name := fqGroupSplit(fq)
	return stem, name, nil
}

func qualify(n string) (*groupInfo, error) {
	fq, err := fqGroupName(n)
	if err != nil {
		return nil, err
	}
	stem, name := fqGroupSplit(fq)
	return &groupInfo{
		Stem: stem,
		Name: name,
	}, nil
}

type groupInfo struct {
	Stem string
	Name string
}

// FullName returns the stem + group string

func (g groupInfo) FullName() string {
	return g.Stem + ":" + g.Name
}

func getUserGroups(cmd cobra.Command) ([]string, []string, error) {
	users := mustGetCmd[[]string](cmd, "user")
	groups := mustGetCmd[[]string](cmd, "group")

	if len(users) == 0 && len(groups) == 0 {
		return nil, nil, errors.New("must specify at least one user or group to add")
	}
	return users, groups, nil
}

func getGroupID(stem, name string, client *grouper.Client) (string, error) {
	g, serr := client.SearchGroups(grouper.WsQueryFilter{
		GroupName:       name,
		QueryFilterType: "FIND_BY_GROUP_NAME_APPROXIMATE",
		StemName:        stem,
	})
	if serr != nil {
		return "", serr
	}
	if len(g) == 0 {
		return "", fmt.Errorf("group not found: %v:%v", stem, name)
	}
	return g[0].UUID, nil
}

func subjectIDsWithCmd(cmd *cobra.Command, args []string, client *grouper.Client) ([]grouper.SubjectLookup, string, error) {
	users, groups, err := getUserGroups(*cmd)
	if err != nil {
		return nil, "", err
	}
	subjectIDs := []grouper.SubjectLookup{}
	for _, uarg := range users {
		res, uerr := client.Subject("subjectIdentifier", uarg)
		if uerr != nil {
			return nil, "", uerr
		}
		subjectIDs = append(subjectIDs, grouper.SubjectLookup{
			SubjectID: res.WsSubjects[0].ID,
		})
	}

	for _, garg := range groups {
		stem, name, qerr := qualifyAndSplit(garg)
		if qerr != nil {
			return nil, "", qerr
		}
		id, gerr := getGroupID(stem, name, client)
		if gerr != nil {
			return nil, "", gerr
		}
		subjectIDs = append(subjectIDs, grouper.SubjectLookup{
			SubjectID: id,
		})
	}

	targetGroup, err := fqGroupName(args[0])
	if err != nil {
		return nil, "", err
	}
	stem, gn := fqGroupSplit(targetGroup)

	tg, err := client.SearchGroups(grouper.WsQueryFilter{
		GroupName:       gn,
		QueryFilterType: "FIND_BY_GROUP_NAME_APPROXIMATE",
		StemName:        stem,
	})
	if err != nil {
		return nil, targetGroup, err
	}
	if len(tg) == 0 {
		return nil, targetGroup, fmt.Errorf("target group not found: %v", targetGroup)
	}
	return subjectIDs, targetGroup, nil
}

func bindUserGroups(cmd *cobra.Command) {
	cmd.PersistentFlags().StringArrayP("user", "u", []string{}, "Users to add")
	cmd.PersistentFlags().StringArrayP("group", "g", []string{}, "Groups to add")
}

func changePrivileges(cmd *cobra.Command, args []string, action bool) error {
	allowed := "T"
	privAction := "add"
	if !action {
		allowed = "F"
		privAction = "remove"
	}
	privs := mustGetCmd[[]string](*cmd, "privilege")
	privType := mustGetCmd[string](*cmd, "type")
	subjectIDs, targetGroup, err := subjectIDsWithCmd(cmd, args, client)
	if err != nil {
		return err
	}

	if !confirm(*cmd,
		"Are you sure you want to change these privileges?",
		fmt.Sprintf("group: %v\nprivs: %v\npriveType: %v\naction: %v", targetGroup, privs, privType, privAction),
	) {
		return errors.New("not confirmed")
	}

	res, err := client.SetPrivilege(grouper.WsRestAssignGrouperPrivilegesRequest{
		WsSubjectLookups:   subjectIDs,
		ReplaceAllExisting: "F",
		WsGroupLookup: grouper.WsGroupLookup{
			GroupName: targetGroup,
		},
		Allowed:        allowed,
		PrivilegeNames: privs,
		PrivilegeType:  privType,
	})
	if err != nil {
		return err
	}
	for _, result := range res.Results {
		logger.Info("completed", "code", result.ResultMetadata.ResultCode, "subject", result.WsSubject.Name, "type", result.PrivilegeType, "name", result.PrivilegeName)
	}

	return nil
}

func confirm(cmd cobra.Command, title, description string) bool {
	if mustGetCmd[bool](cmd, "skip-confirm") {
		logger.Info("skipping confirmation since --skip-confirm was given")
		return true
	}
	var confirmed bool
	form := huh.NewForm(
		huh.NewGroup(
			huh.NewConfirm().
				Title(title).
				Description(description).
				Value(&confirmed),
		),
	)
	if err := form.Run(); err != nil {
		panic(err)
	}
	return confirmed
}

/*
func toPTR[V any](v V) *V {
	return &v
}
*/

func fromPTR[V any](v *V) V {
	return *v
}
