package cmd

import "github.com/spf13/cobra"

func newCreateCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "create",
		Aliases: []string{"add"},
		Args:    cobra.ExactArgs(1),
		Short:   "Add something to Grouper",
	}
	cmd.AddCommand(
		newCreateMembersCmd(),
		newCreateGroupsCmd(),
		newCreatePrivilegesCmd(),
	)
	return cmd
}
