package cmd

import (
	"fmt"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newGetPrivilegesCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "privileges GROUP",
		Aliases: []string{"privilege", "p", "priv"},
		Args:    cobra.MinimumNArgs(1),
		Short:   "Return privilege information about a given group",
		RunE: func(cmd *cobra.Command, args []string) error {
			gi, err := qualify(args[0])
			if err != nil {
				return err
			}
			privs, err := client.Privileges(grouper.WsRestGetGrouperPrivilegesLiteRequest{
				GroupName: gi.FullName(),
			})
			if err != nil {
				return err
			}
			if mustGetCmd[bool](*cmd, "summary") {
				fmt.Print(privs.PrivilegeResults.Summary())
			} else {
				gout.MustPrint(privs.PrivilegeResults)
			}
			return nil
		},
	}
	return cmd
}
