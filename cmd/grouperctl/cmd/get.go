package cmd

import "github.com/spf13/cobra"

func newGetCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "get",
		Args:  cobra.ExactArgs(1),
		Short: "Get something out of grouper",
	}
	cmd.AddCommand(
		newGetGroupsCmd(),
		newGetMembersCmd(),
		newGetUsersCmd(),
		newGetPrivilegesCmd(),
		newGetSubjectCmd(),
	)
	cmd.PersistentFlags().Bool("summary", false, "Only print out a brief summary of the group membership")
	return cmd
}
