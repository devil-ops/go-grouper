package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newRemoveMembersCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "members GROUP",
		Short:   "Remove members from a group",
		Aliases: []string{"member"},
		Args:    cobra.ExactArgs(1),
		RunE:    runRemoveMembers,
	}
	bindUserGroups(cmd)
	return cmd
}

func runRemoveMembers(cmd *cobra.Command, args []string) error {
	subjectIDs, targetGroup, err := subjectIDsWithCmd(cmd, args, client)
	if err != nil {
		return err
	}

	if !confirm(*cmd, "Are you sure you want to these members?", fmt.Sprintf("group: %v\nids: %v", targetGroup, subjectIDs)) {
		return errors.New("not confirmed")
	}

	res, err := client.RemoveMember(targetGroup, grouper.WsRestDeleteMemberRequest{
		ReplaceAllExisting: "F",
		SubjectLookups:     subjectIDs,
	})
	if err != nil {
		return err
	}
	for _, result := range res.Results {
		logger.Info("completed", "code", result.ResultMetadata.ResultCode, "name", result.WsSubject.Name)
	}

	return nil
}
