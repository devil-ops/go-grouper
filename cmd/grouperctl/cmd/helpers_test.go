package cmd

import (
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestFQSplit(t *testing.T) {
	s, n := fqGroupSplit("foo:bar:baz")
	assert.Equal(t, "foo:bar", s)
	assert.Equal(t, "baz", n)
}

func TestFQGroupName(t *testing.T) {
	tests := map[string]struct {
		given     string
		givenStem string
		expect    string
		expectErr string
	}{
		"alreadyfqdn": {
			given:  "foo:bar",
			expect: "foo:bar",
		},
		"fromstem": {
			given:     "bar",
			givenStem: "foo",
			expect:    "foo:bar",
		},
		"fromstemsub": {
			given:     "bar",
			givenStem: "foo:baz",
			expect:    "foo:baz:bar",
		},
		"missingstem": {
			given:     "bar",
			expectErr: "must include a full stem (include :)",
		},
	}

	for desc, tt := range tests {
		viper.Set("stem", tt.givenStem)
		got, err := fqGroupName(tt.given)
		if tt.expectErr == "" {
			require.NoError(t, err, desc)
			require.Equal(t, tt.expect, got, desc)
		} else {
			require.EqualError(t, err, tt.expectErr, desc)
		}
		viper.Reset()
	}
}

func TestQualifyAndSplit(t *testing.T) {
	tests := map[string]struct {
		given      string
		givenStem  string
		expectStem string
		expectName string
		expectErr  string
	}{
		"good": {
			given:      "bar",
			givenStem:  "foo",
			expectStem: "foo",
			expectName: "bar",
		},
		"bad": {
			given:     "bar",
			expectErr: "must include a full stem (include :)",
		},
	}

	for desc, tt := range tests {
		viper.Set("stem", tt.givenStem)
		gotStem, gotName, err := qualifyAndSplit(tt.given)
		if tt.expectErr == "" {
			require.NoError(t, err, desc)
			require.Equal(t, tt.expectStem, gotStem)
			require.Equal(t, tt.expectName, gotName)
		} else {
			require.EqualError(t, err, tt.expectErr, desc)
		}
		viper.Reset()
	}
}

func TestQualify(t *testing.T) {
	viper.Set("stem", "foo:bar")
	got, err := qualify("baz")
	require.NoError(t, err)
	require.Equal(
		t,
		&groupInfo{
			Stem: "foo:bar",
			Name: "baz",
		},
		got,
	)
	require.Equal(t, "foo:bar:baz", got.FullName())
	viper.Reset()

	_, err = qualify("baz")
	require.EqualError(t, err, "must include a full stem (include :)")
}
