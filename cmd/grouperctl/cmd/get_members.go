package cmd

import (
	"fmt"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newGetMembersCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "members",
		Aliases: []string{"m", "member"},
		Args:    cobra.ExactArgs(1),
		Short:   "Get group membership",
		RunE: func(cmd *cobra.Command, args []string) error {
			gi, err := qualify(args[0])
			if err != nil {
				return err
			}

			mf := "Immediate"
			if mustGetCmd[bool](*cmd, "all") {
				mf = "ALL"
			}

			logger.Info("querying grouper", "stem", gi.Stem, "group", gi.Name)
			groups, err := client.Members(fromPTR(
				grouper.NewGetMembersRequest(
					grouper.WithMemberFilter(mf),
					grouper.WithGroupLookup(grouper.WsGroupLookup{
						GroupName: gi.FullName(),
					}),
				),
			))
			if err != nil {
				return err
			}

			if mustGetCmd[bool](*cmd, "summary") {
				for _, item := range groups {
					fmt.Print(item.Summary())
				}
			} else {
				gout.MustPrint(groups)
			}
			logger.Info("completed", "found", len(groups))
			return nil
		},
	}
	cmd.PersistentFlags().StringP("stem", "s", "", "Search only under the given stem. Set the env variable GROUPERCTL_STEM if you get sick of adding this every time")
	cmd.PersistentFlags().BoolP("all", "a", false, "Include all results, not just immediate results")
	return cmd
}
