package cmd

import (
	"fmt"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newGetSubjectCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "subject ID",
		Args:    cobra.MinimumNArgs(1),
		Short:   "Look up subjects in Grouper. A subject can be either a user, group, or probably other thing that I'm not aware of",
		Aliases: []string{"subjects"},
		RunE:    getSubject,
	}
	cmd.PersistentFlags().StringP("by", "b", "subjectIdentifier", "Field to search on. Examples: subjectId looks up based off of a unique ID, subjectIdentifier looks up based off of the name field")
	return cmd
}

func getSubject(cmd *cobra.Command, args []string) error {
	ret := make(grouper.WsSubjectList, len(args))
	for idx, item := range args {
		subjects, err := client.Subject(mustGetCmd[string](*cmd, "by"), item)
		if err != nil {
			return err
		}
		ret[idx] = subjects.WsSubjects[0]
	}
	if mustGetCmd[bool](*cmd, "summary") {
		fmt.Print(ret.Summary())
	} else {
		gout.MustPrint(ret)
	}
	return nil
}
