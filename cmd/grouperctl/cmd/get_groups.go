package cmd

import (
	"fmt"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newGetGroupsCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "groups [GROUP_NAME...]",
		Aliases: []string{"g", "group"},
		Short:   "Get group listings out of Grouper",
		RunE: func(cmd *cobra.Command, args []string) error {
			targets := []string{"%"}
			if len(args) != 0 {
				targets = args
			}

			ret := grouper.GroupList{}
			stem := viper.GetString("stem")
			count := 0
			for _, name := range targets {
				logger.Info("querying grouper", "stem", stem, "name", name)
				groups, err := client.SearchGroups(fromPTR(
					grouper.NewQueryFilter(
						grouper.WithGroupName[grouper.WsQueryFilter](name),
						grouper.WithStem(stem),
					)))
				if err != nil {
					return err
				}
				for _, group := range groups {
					ret = append(ret, group)
					count++
				}
			}
			if mustGetCmd[bool](*cmd, "summary") {
				fmt.Print(ret.Summary())
			} else {
				gout.MustPrint(ret)
			}
			logger.Info("completed", "found", count)
			return nil
		},
	}
	cmd.PersistentFlags().StringP("stem", "s", "", "Search only under the given stem. Set the env variable GROUPERCTL_STEM if you get sick of adding this every time")
	panicIfErr(viper.BindPFlags(cmd.PersistentFlags()))
	return cmd
}
