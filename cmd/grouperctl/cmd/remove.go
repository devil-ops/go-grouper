package cmd

import "github.com/spf13/cobra"

func newRemoveCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "remove",
		Aliases: []string{"delete", "del", "rem"},
		Args:    cobra.ExactArgs(1),
		Short:   "Remove something from Grouper",
	}
	cmd.AddCommand(
		newRemoveMembersCmd(),
		newRemoveGroupsCmd(),
		newRemovePrivilegesCmd(),
	)
	return cmd
}
