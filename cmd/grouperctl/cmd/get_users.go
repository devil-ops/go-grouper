package cmd

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newGetUsersCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "users [NetID|DukeID...]",
		Aliases: []string{"u", "user"},
		Args:    cobra.MinimumNArgs(1),
		Short:   "Get information about a user",
		RunE: func(_ *cobra.Command, args []string) error {
			ret := make([]grouper.WsSubject, len(args))
			for idx, item := range args {
				u, err := client.Subject("subjectIdentifier", item)
				if err != nil {
					return err
				}
				ret[idx] = u.WsSubjects[0]
			}
			gout.MustPrint(ret)
			return nil
		},
	}
	return cmd
}
