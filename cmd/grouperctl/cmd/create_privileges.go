package cmd

import (
	"github.com/spf13/cobra"
)

func newCreatePrivilegesCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "privileges GROUP",
		Aliases: []string{"privilege", "priv", "p"},
		Short:   "Add privileges to a given group",
		Args:    cobra.ExactArgs(1),
		RunE:    runCreatePrivileges,
	}
	bindUserGroups(cmd)
	cmd.PersistentFlags().StringArray("privilege", []string{"admin", "read"}, "Privileges to grant to a user or group")
	cmd.PersistentFlags().String("type", "access", "What type of access to grant")
	return cmd
}

func runCreatePrivileges(cmd *cobra.Command, args []string) error {
	return changePrivileges(cmd, args, true)
}
