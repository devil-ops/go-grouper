package cmd

import (
	"errors"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newRemoveGroupsCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "group NAME",
		Short:   "Remove groups from Grouper",
		Aliases: []string{"groups"},
		Args:    cobra.ExactArgs(1),
		RunE:    runRemoveGroups,
	}
	return cmd
}

func runRemoveGroups(cmd *cobra.Command, args []string) error {
	gi, err := qualify(args[0])
	if err != nil {
		return err
	}
	if !confirm(*cmd,
		"Are you sure you want to delete this group?",
		fmt.Sprintf("name: %v\nstem: %v", gi.Name, gi.Stem),
	) {
		return errors.New("not confirmed")
	}
	res, err := client.Remove(grouper.NewDeleteGroupRequest(gi.FullName()))
	if err != nil {
		return err
	}
	for _, item := range res.Results {
		logger.Info(strings.ReplaceAll(item.ResultMetadata.ResultMessage, "\n", " "), "code", item.ResultMetadata.ResultCode)
	}
	return nil
}
