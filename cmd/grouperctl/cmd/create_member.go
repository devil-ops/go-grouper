package cmd

import (
	"github.com/spf13/cobra"
	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func newCreateMembersCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "member GROUP MEMBER",
		Aliases: []string{"m", "members"},
		Short:   "Add a member to a given Grouper group",
		Args:    cobra.ExactArgs(1),
		RunE:    runCreateMembers,
	}
	bindUserGroups(cmd)
	return cmd
}

func runCreateMembers(cmd *cobra.Command, args []string) error {
	subjectIDs, targetGroup, err := subjectIDsWithCmd(cmd, args, client)
	if err != nil {
		return err
	}

	res, err := client.AddMember(targetGroup, grouper.WsRestAddMemberRequest{
		ReplaceAllExisting: "F",
		SubjectLookups:     subjectIDs,
	})
	if err != nil {
		return err
	}
	for _, result := range res.Results {
		logger.Info("completed", "code", result.ResultMetadata.ResultCode, "name", result.WsSubject.Name)
	}

	return nil
}
