/*
Package main is the main executable for the CLI
*/
package main

import "gitlab.oit.duke.edu/devil-ops/go-grouper/cmd/grouperctl/cmd"

func main() {
	cmd.Execute()
}
