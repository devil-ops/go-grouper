package main

import (
	"fmt"
	"log"
	"os"

	grouper "gitlab.oit.duke.edu/devil-ops/go-grouper"
)

func main() {
	c := grouper.MustNew(
		grouper.WithUsername(os.Getenv("GROUPER_USERNAME")),
		grouper.WithPassword(os.Getenv("GROUPER_PASSWORD")),
		grouper.WithDebugCurl(),
	)
	req := grouper.WsQueryFilter{
		GroupName:       "%",
		QueryFilterType: "FIND_BY_GROUP_NAME_APPROXIMATE",
		StemName:        "duke:oit:systems:unix",
	}

	got, err := c.SearchGroups(req)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(os.Stdout, "%+v\n", got)
}
