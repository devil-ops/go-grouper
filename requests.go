package grouper

import (
	"strings"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

// WsRestGetMembersRequestWrapper wraps the request for members
type WsRestGetMembersRequestWrapper struct {
	WsRestGetMembersRequest WsRestGetMembersRequest `json:"WsRestGetMembersRequest,omitempty"`
}

// WithGroupLookup filters the group lookup by group names
func WithGroupLookup(l WsGroupLookup) func(*WsRestGetMembersRequest) {
	return func(r *WsRestGetMembersRequest) {
		r.WsGroupLookups = append(r.WsGroupLookups, l)
	}
}

// WithMemberFilter sets the membership filter on a new get members request
func WithMemberFilter(m string) func(*WsRestGetMembersRequest) {
	return func(r *WsRestGetMembersRequest) {
		r.MemberFilter = m
	}
}

// NewGetMembersRequest returns a new member request item using functional options
func NewGetMembersRequest(opts ...func(*WsRestGetMembersRequest)) *WsRestGetMembersRequest {
	r := &WsRestGetMembersRequest{
		WsGroupLookups:        []WsGroupLookup{},
		MemberFilter:          "Immediate",
		SubjectAttributeNames: []string{"uid"},
		IncludeSubjectDetail:  "F",
	}
	for _, opt := range opts {
		opt(r)
	}
	return r
}

// WsRestGetMembersRequest returns the member request info
type WsRestGetMembersRequest struct {
	IncludeSubjectDetail  string          `json:"includeSubjectDetail,omitempty"`
	MemberFilter          string          `json:"memberFilter,omitempty"`
	SubjectAttributeNames []string        `json:"subjectAttributeNames,omitempty"`
	WsGroupLookups        []WsGroupLookup `json:"wsGroupLookups,omitempty"`
}

// WsGroupLookup is an item returned from a group lookup
type WsGroupLookup struct {
	GroupName string `json:"groupName,omitempty"`
}

// WsRestFindGroupsRequestWrapper is what is sent to find groups
type WsRestFindGroupsRequestWrapper struct {
	WsRestFindGroupsRequest WsRestFindGroupsRequest `json:"WsRestFindGroupsRequest,omitempty"`
}

// WsRestFindGroupsRequest handles the API for the request
type WsRestFindGroupsRequest struct {
	ActAsSubjectLookup *ActAsSubjectLookup `json:"subjectId,omitempty"`
	WsQueryFilter      WsQueryFilter       `json:"wsQueryFilter,omitempty"`
}

// ActAsSubjectLookup defines how to act in a lookup
type ActAsSubjectLookup struct {
	SubjectID string `json:"subjectId,omitempty"`
}

// NewQueryFilter returns a new WsQueryFilter item with functional options
func NewQueryFilter(opts ...func(*WsQueryFilter)) *WsQueryFilter {
	f := &WsQueryFilter{
		GroupName:       "%",
		QueryFilterType: "FIND_BY_GROUP_NAME_APPROXIMATE",
	}
	for _, opt := range opts {
		opt(f)
	}
	return f
}

// WithQueryFilterType sets the QueryFilterType on a new WsQueryFilter item
func WithQueryFilterType(q string) func(*WsQueryFilter) {
	return func(w *WsQueryFilter) {
		w.QueryFilterType = q
	}
}

// WithStem sets the stem on a new WsQueryFilter item
func WithStem(s string) func(*WsQueryFilter) {
	return func(w *WsQueryFilter) {
		w.StemName = s
	}
}

// WsQueryFilter is for the actual query filter
type WsQueryFilter struct {
	GroupName       string `json:"groupName,omitempty"`
	QueryFilterType string `json:"queryFilterType,omitempty"`
	StemName        string `json:"stemName,omitempty"`
}

// AddMemberRequestWrapper is the request to add a member
type AddMemberRequestWrapper struct {
	WsRestAddMemberRequest WsRestAddMemberRequest
}

// WsRestAddMemberRequest is the actual request data
type WsRestAddMemberRequest struct {
	ReplaceAllExisting string          `json:"replaceAllExisting,omitempty"`
	SubjectLookups     []SubjectLookup `json:"subjectLookups,omitempty"`
}

// SubjectLookup describes the subject thing to look up
type SubjectLookup struct {
	SubjectID string `json:"subjectId,omitempty"`
}

// DeleteMemberRequestWrapper removes members from a group
type DeleteMemberRequestWrapper struct {
	WsRestDeleteMemberRequest WsRestDeleteMemberRequest
}

// WsRestDeleteMemberRequest is the actual request data
type WsRestDeleteMemberRequest struct {
	ReplaceAllExisting string          `json:"replaceAllExisting,omitempty"`
	SubjectLookups     []SubjectLookup `json:"subjectLookups,omitempty"`
}

// AddGroupRequestWrapper is what we send to add a new group
type AddGroupRequestWrapper struct {
	WsRestGroupSaveLiteRequest WsRestGroupSaveLiteRequest
}

// WithGroupName sets the group name on an item
func WithGroupName[T WsRestGroupSaveLiteRequest | WsQueryFilter](s string) func(*T) {
	return func(w *T) {
		switch x := any(w).(type) {
		case *WsRestGroupSaveLiteRequest:
			x.GroupName = s
		case *WsQueryFilter:
			x.GroupName = s
		default:
			panic("Unsupported generic usage...how did we get here?")
		}
	}
	/*
		return func(w *WsRestGroupSaveLiteRequest) {
			w.GroupName = s
		}
	*/
}

// WithDescription sets the description on an item
func WithDescription(s string) func(*WsRestGroupSaveLiteRequest) {
	return func(w *WsRestGroupSaveLiteRequest) {
		w.Description = s
	}
}

// WithDisplayExtension sets the display extension on a new object
func WithDisplayExtension(s string) func(*WsRestGroupSaveLiteRequest) {
	return func(w *WsRestGroupSaveLiteRequest) {
		w.DisplayExtension = s
	}
}

// NewDeleteGroupRequest deletes creates a new delete request for the given VMs
// Pass in a list of fully qualified group names, and they will be deleted
func NewDeleteGroupRequest(groups ...string) WsRestGroupDeleteRequest {
	gls := make([]WsGroupLookup, len(groups))
	for idx, group := range groups {
		gls[idx] = WsGroupLookup{
			GroupName: group,
		}
	}
	return WsRestGroupDeleteRequest{
		WsGroupLookups: gls,
	}
}

// NewCreateGroupRequest returns a WsRestGroupSaveLiteRequest with information on how to create a group
func NewCreateGroupRequest(opts ...func(*WsRestGroupSaveLiteRequest)) *WsRestGroupSaveLiteRequest {
	r := &WsRestGroupSaveLiteRequest{}
	for _, opt := range opts {
		opt(r)
	}
	if r.GroupName == "" {
		panic("Must set a group name")
	}
	if r.Description == "" {
		// r.Description = r.GroupName
		r.Description = r.GroupName[strings.LastIndex(r.GroupName, ":")+1:]
	}
	if r.DisplayExtension == "" {
		caser := cases.Title(language.English)
		r.DisplayExtension = caser.String(
			r.GroupName[strings.LastIndex(r.GroupName, ":")+1:],
		)
	}
	return r
}

// WsRestGroupSaveLiteRequest is the actual request for a new group
type WsRestGroupSaveLiteRequest struct {
	Description      string `json:"description,omitempty"`
	DisplayExtension string `json:"displayExtension,omitempty"`
	GroupName        string `json:"groupName,omitempty"`
}

// DeleteGroupRequestWrapper is the raw request sent to delete a group
type DeleteGroupRequestWrapper struct {
	WsRestGroupDeleteRequest WsRestGroupDeleteRequest
}

// WsRestGroupDeleteRequest is the actual request for the delete
type WsRestGroupDeleteRequest struct {
	WsGroupLookups []WsGroupLookup `json:"wsGroupLookups,omitempty"`
}

// ListPrivilegesRequestWrapper is what is sent to get a list of privileges
type ListPrivilegesRequestWrapper struct {
	WsRestGetGrouperPrivilegesLiteRequest WsRestGetGrouperPrivilegesLiteRequest
}

// WsRestGetGrouperPrivilegesLiteRequest is the actual juicy part of the request
type WsRestGetGrouperPrivilegesLiteRequest struct {
	GroupName     string `json:"groupName,omitempty"`
	PrivilegeName string `json:"privilegeName,omitempty"`
	PrivilegeType string `json:"privilegeType,omitempty"`
}

// AssignPrivilegesRequestWrapper is what we send when assigning privileges
type AssignPrivilegesRequestWrapper struct {
	WsRestAssignGrouperPrivilegesRequest WsRestAssignGrouperPrivilegesRequest
}

// WsRestAssignGrouperPrivilegesRequest is the juicy part of the request
type WsRestAssignGrouperPrivilegesRequest struct {
	Allowed            string          `json:"allowed,omitempty"`
	PrivilegeNames     []string        `json:"privilegeNames,omitempty"`
	PrivilegeType      string          `json:"privilegeType,omitempty"`
	ReplaceAllExisting string          `json:"replaceAllExisting,omitempty"`
	WsGroupLookup      WsGroupLookup   `json:"wsGroupLookup,omitempty"`
	WsSubjectLookups   []SubjectLookup `json:"wsSubjectLookups,omitempty"`
}
