package grouper

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewCreateGroupRequest(t *testing.T) {
	got := NewCreateGroupRequest(WithGroupName[WsRestGroupSaveLiteRequest]("some-group"))
	require.Equal(t, "some-group", got.GroupName)
	require.Equal(t, "some-group", got.Description)
	require.Equal(t, "Some-Group", got.DisplayExtension)
}

func TestNewDeleteGroupRequest(t *testing.T) {
	require.Equal(
		t,
		WsRestGroupDeleteRequest{
			WsGroupLookups: []WsGroupLookup{
				{GroupName: "foo:bar"},
				{GroupName: "baz:bing"},
			},
		},
		NewDeleteGroupRequest("foo:bar", "baz:bing"),
	)
}

func TestNewGetMembersRequest(t *testing.T) {
	require.Equal(
		t,
		&WsRestGetMembersRequest{
			IncludeSubjectDetail:  "F",
			MemberFilter:          "Immediate",
			SubjectAttributeNames: []string{"uid"},
			WsGroupLookups: []WsGroupLookup{
				{GroupName: "foo:bar"},
			},
		},
		NewGetMembersRequest(WithGroupLookup(WsGroupLookup{GroupName: "foo:bar"})),
	)
}

func TestNewFilterRequest(t *testing.T) {
	require.Equal(
		t,
		&WsQueryFilter{
			GroupName:       "foo",
			QueryFilterType: "FIND_BY_GROUP_NAME_APPROXIMATE",
		},
		NewQueryFilter(WithGroupName[WsQueryFilter]("foo")),
	)
}
